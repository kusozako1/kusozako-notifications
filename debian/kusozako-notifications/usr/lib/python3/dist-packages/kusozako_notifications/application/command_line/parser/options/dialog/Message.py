# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_notifications.alfa.MessageEntity import AlfaMessageEntity

TEMPLATE = "<span font-weight='bold'>{}</span>"


class FoxtrotMessage(AlfaMessageEntity):

    @classmethod
    def new(cls, options_dict, option_value):
        instance = cls()
        instance.construct(options_dict, option_value)
        return instance

    def construct(self, options_dict, option_value):
        self._markup = TEMPLATE.format(option_value.get_string())
        extra_buttons_raw = options_dict.lookup_value(GLib.OPTION_REMAINING)
        if extra_buttons_raw is None:
            self._extra_buttons = []
        else:
            self._extra_buttons = extra_buttons_raw.get_strv()

    @property
    def markup(self):
        return self._markup

    @property
    def extra_buttons(self):
        return self._extra_buttons

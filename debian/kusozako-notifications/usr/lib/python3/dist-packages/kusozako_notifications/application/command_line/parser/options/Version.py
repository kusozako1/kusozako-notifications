# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity


class DeltaVersion(DeltaEntity):

    def execute(self, options_dict, option_value):
        print("version : ", "2023.11.10")

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > add option", ("version", self))

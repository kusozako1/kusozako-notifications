# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib


class FoxtrotResources:

    def get_path(self, resource_name):
        directory_name = GLib.path_get_dirname(__file__)
        return GLib.build_filenamev([directory_name, resource_name])

    def __init__(self):
        directory_name = GLib.path_get_dirname(__file__)
        css_path = GLib.build_filenamev([directory_name, "application.css"])
        css_provider = Gtk.CssProvider()
        css_provider.load_from_path(css_path)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )

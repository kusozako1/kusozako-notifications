# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_notifications.Entity import DeltaEntity

DIALOG = (
    "dialog",
    ord("d"),
    GLib.OptionFlags.NONE,
    GLib.OptionArg.STRING,
    "dialog mode",
    "message to show"
)

BUTTONS = (
    GLib.OPTION_REMAINING,
    0,
    GLib.OptionFlags.NONE,
    GLib.OptionArg.STRING_ARRAY,
    "button/action pair for dialog",
    None
)


VERSION = (
    "version",
    ord("V"),
    GLib.OptionFlags.NONE,
    GLib.OptionArg.NONE,
    "show version",
    None
)


class DeltaBuilder(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        application = self._enquiry("delta > application")
        application.add_main_option(*BUTTONS)
        application.add_main_option(*DIALOG)
        application.add_main_option(*VERSION)
        application.set_option_context_summary("kusozako-notifications")
        self._raise("delta > options built", application)

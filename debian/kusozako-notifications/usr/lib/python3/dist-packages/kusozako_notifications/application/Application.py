# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
from gi.repository import Gio
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.Transmitter import FoxtrotTransmitter
from .resources.Resources import FoxtrotResources
from .command_line.CommandLine import DeltaCommandLine


class DeltaApplication(Gio.Application, DeltaEntity):

    def _delta_info_resource_path(self, resource_name):
        return self._resources.get_path(resource_name)

    def _delta_call_application_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_application_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_info_application(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gio.Application.__init__(
            self,
            application_id="com.gitlab.kusozako1.Notifications",
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            )
        DeltaCommandLine(self)
        self._resources = FoxtrotResources()
        self._transmitter = FoxtrotTransmitter()
        self._raise("delta > application layer ready", self)
        self.run(sys.argv)

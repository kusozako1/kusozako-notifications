# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('GSound', '1.0')
gi.require_version('GtkLayerShell', '0.1')

VERSION = "2024.07.18"
VENDOR_NAME = "kusozako-tech"
APPLICATION_NAME = "kusozako-notifications"
APPLICATION_ID = "com.gitlab.kusozako1.Notifications"

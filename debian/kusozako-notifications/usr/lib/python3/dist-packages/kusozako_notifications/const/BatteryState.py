# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MESSAGES = {
    0: "UNKNOWN STATE",
    1: "Battery Charging",
    2: "Battery Discharging",
    3: "Battery Empty",
    4: "Battery Fully Charged",
    5: "Battery Pending Charge",
    6: "Battery Pending Discharge"
}

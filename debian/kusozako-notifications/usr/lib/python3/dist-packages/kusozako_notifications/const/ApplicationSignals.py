# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

ENQUEUE_MESSAGE = "enqueue-mesage"              # MessageEntity
QUEUE_MESSAGE = "queue-message"                 # MessageEntity or None
DEQUEUE_MESSAGE = "dequeue-messagge"            # None

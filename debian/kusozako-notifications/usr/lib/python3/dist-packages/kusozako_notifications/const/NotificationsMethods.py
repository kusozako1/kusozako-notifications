# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

GET_SERVER_INFORMATION = "GetServerInformation"
GET_CAPABILITIES = "GetCapabilities"
CLOSE_NOTIFICATION = "CloseNotification"
NOTIFY = "Notify"

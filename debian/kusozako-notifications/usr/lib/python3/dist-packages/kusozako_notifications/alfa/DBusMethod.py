# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity


class AlfaDBusMethod(DeltaEntity):

    __method_name__ = "define method name here."

    def _invoke(self, param, invocation):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        method, param, invocation = user_data
        if method != self.__method_name__:
            return
        self._invoke(param, invocation)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register dbus method object", self)

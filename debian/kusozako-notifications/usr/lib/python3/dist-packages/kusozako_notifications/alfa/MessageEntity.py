# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject
from kusozako_notifications.const import NotifyUrgency


class AlfaMessageEntity(GObject.Object):

    def close(self):
        pass

    def __init__(self):
        GObject.Object.__init__(self)

    @property
    def icon(self):
        return None

    @property
    def markup(self):
        return ""

    @property
    def urgency(self):
        return NotifyUrgency.NORMAL

    @property
    def extra_buttons(self):
        return []

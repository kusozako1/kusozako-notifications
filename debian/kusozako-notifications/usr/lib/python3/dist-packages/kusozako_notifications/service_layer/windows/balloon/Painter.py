# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_notifications.const import BalloonAllocation


def _draw_arc_rectangle(cairo_context):
    left = BalloonAllocation.RADIUS+BalloonAllocation.TRIANGLE_OFFSET
    top = BalloonAllocation.RADIUS
    right = BalloonAllocation.WIDTH-BalloonAllocation.RADIUS
    bottom = BalloonAllocation.HEIGHT-BalloonAllocation.RADIUS
    radius = BalloonAllocation.RADIUS
    cairo_context.arc(left, top, radius, 2*(GLib.PI_2), 3*(GLib.PI_2))
    cairo_context.arc(right, top, radius, 3*(GLib.PI_2), 4*(GLib.PI_2))
    cairo_context.arc(right, bottom, radius, 0*(GLib.PI_2), 1*(GLib.PI_2))
    cairo_context.arc(left, bottom, radius, 1*(GLib.PI_2), 2*(GLib.PI_2))
    cairo_context.close_path()
    cairo_context.fill()


def _draw_triangle(cairo_context):
    cairo_context.move_to(0, 100)
    cairo_context.line_to(BalloonAllocation.TRIANGLE_OFFSET, 60)
    cairo_context.line_to(BalloonAllocation.TRIANGLE_OFFSET, 80)
    cairo_context.fill()


def paint(cairo_context):
    _draw_arc_rectangle(cairo_context)
    _draw_triangle(cairo_context)

# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_notifications.Entity import DeltaEntity
from .extra_buttons.ExtraButtons import DeltaExtraButtons
from .CancelButton import DeltaCancelButton


class DeltaButtonBox(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ButtonBox.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaExtraButtons(self)
        DeltaCancelButton(self)
        self._raise("delta > add to container", self)

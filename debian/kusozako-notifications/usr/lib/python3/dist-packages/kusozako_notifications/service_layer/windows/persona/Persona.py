# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GtkLayerShell
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals


class DeltaPersona(Gtk.Window, DeltaEntity):

    def _on_draw(self, drawing_area, cairo_context, pixbuf):
        Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, 0, 0)
        cairo_context.paint()

    def _on_realize(self, window):
        pixbuf = self._enquiry("delta > pixbuf")
        if pixbuf is None:
            return
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        self.set_size_request(width, height)
        GtkLayerShell.init_for_window(window)
        GtkLayerShell.set_anchor(window, GtkLayerShell.Edge.LEFT, True)
        GtkLayerShell.set_anchor(window, GtkLayerShell.Edge.BOTTOM, True)
        GtkLayerShell.set_margin(window, GtkLayerShell.Edge.BOTTOM, -45)
        GtkLayerShell.set_layer(window, GtkLayerShell.Layer.OVERLAY)
        self.connect("draw", self._on_draw, pixbuf)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def receive_transmission(self, user_data):
        signal, message = user_data
        if signal != ApplicationSignals.QUEUE_MESSAGE:
            return
        pixbuf = self._enquiry("delta > pixbuf")
        if pixbuf is None:
            return
        self.props.visible = (message is not None)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Window.__init__(self)
        self._drawing_area = Gtk.DrawingArea(hexpand=True, vexpand=True)
        self.add(self._drawing_area)
        self.connect("realize", self._on_realize)
        self._raise("delta > register application object", self)

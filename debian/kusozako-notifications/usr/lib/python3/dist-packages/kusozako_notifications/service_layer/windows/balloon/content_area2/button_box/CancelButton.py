# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals


class DeltaCancelButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = ApplicationSignals.DEQUEUE_MESSAGE, None
        self._raise("delta > application signal", user_data)

    def receive_transmission(self, user_data):
        signal, message = user_data
        if signal != ApplicationSignals.QUEUE_MESSAGE or message is None:
            return
        label = "Cancel" if message.extra_buttons else "Close"
        self.set_label(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            "Close",
            hexpand=True,
            relief=Gtk.ReliefStyle.NONE
            )
        self.set_size_request(-1, 48)
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register application object", self)

# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals
from .Message import FoxtrotMessage


class DeltaBatteryDevice(DeltaEntity):

    @classmethod
    def new_for_proxy(cls, parent, device_proxy):
        instance = cls(parent)
        instance.construct(device_proxy)

    def _state_changed(self, state, device_proxy):
        message = FoxtrotMessage.new(state)
        user_data = ApplicationSignals.ENQUEUE_MESSAGE, message
        self._raise("delta > application signal", user_data)

    def _timeout(self, device_proxy):
        device_proxy.Refresh()
        state = device_proxy.get_cached_property("State").get_uint32()
        if state != self._current_state:
            self._state_changed(state, device_proxy)
        self._current_state = state
        return GLib.SOURCE_CONTINUE

    def construct(self, proxy):
        self._current_state = proxy.get_cached_property("State").get_uint32()
        GLib.timeout_add_seconds(1, self._timeout, proxy)

    def __init__(self, parent):
        self._parent = parent

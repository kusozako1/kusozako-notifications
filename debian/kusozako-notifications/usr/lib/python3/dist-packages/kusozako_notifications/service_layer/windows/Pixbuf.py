# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GdkPixbuf
from kusozako_notifications.Entity import DeltaEntity


class DeltaPixbuf(DeltaEntity):

    def get_pixbuf(self):
        return self._pixbuf

    def __init__(self, parent):
        self._parent = parent
        path = self._enquiry("delta > config", ("persona", "path"))
        if GLib.file_test(path, GLib.FileTest.EXISTS):
            self._pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
        else:
            self._pixbuf = None

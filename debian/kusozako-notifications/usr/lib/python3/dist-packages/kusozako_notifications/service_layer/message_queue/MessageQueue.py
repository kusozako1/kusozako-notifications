# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.alfa.MessageEntity import AlfaMessageEntity
from kusozako_notifications.const import ApplicationSignals
from .observers.Observers import EchoObservers


class DeltaMessageQueueu(Gio.ListStore, DeltaEntity):

    def _compare_func(self, alfa, bravo, user_data=None):
        return alfa.urgency - bravo.urgency

    def _delta_call_add_message(self, message):
        self.insert_sorted(message, self._compare_func, None)
        if self.get_n_items() == 1:
            param = ApplicationSignals.QUEUE_MESSAGE, self[0]
            self._raise("delta > application signal", param)

    def _delta_call_remove_message(self, message=None):
        if message is None:
            current_message = self[0]
            self.remove(0)
        else:
            found, position = self.find(message)
            if not found:
                return
            current_message = self[position]
            self.remove(position)
        next_message = self[0] if self.get_n_items() > 0 else None
        param = ApplicationSignals.QUEUE_MESSAGE, next_message
        self._raise("delta > application signal", param)
        # to call dbus-invocation in current entity.
        current_message.close()

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=AlfaMessageEntity)
        EchoObservers(self)

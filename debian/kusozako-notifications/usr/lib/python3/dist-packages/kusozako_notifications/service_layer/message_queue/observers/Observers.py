# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Enqueue import DeltaEnqueue
from .Dequeue import DeltaDequeue


class EchoObservers:

    def __init__(self, parent):
        DeltaEnqueue(parent)
        DeltaDequeue(parent)

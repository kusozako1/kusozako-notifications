# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity
from .message_queue.MessageQueue import DeltaMessageQueueu
from .notifications.Notifications import DeltaNotifications
from .windows.Windows import DeltaWindows
from .SoundContext import DeltaSoundContext
from .battery_watcher.BatteryWatcher import DeltaBatteryWatcher


class DeltaServiceLayer(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        DeltaMessageQueueu(self)
        DeltaNotifications(self)
        DeltaWindows(self)
        DeltaSoundContext(self)
        query = "service", "battery_monitor"
        activate_battery_monitor = self._enquiry("delta > config", query)
        if activate_battery_monitor in (1, True):
            DeltaBatteryWatcher(self)

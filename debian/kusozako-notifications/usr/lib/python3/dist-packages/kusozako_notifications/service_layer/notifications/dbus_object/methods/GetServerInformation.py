# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
import kusozako_notifications
from kusozako_notifications.alfa.DBusMethod import AlfaDBusMethod
from kusozako_notifications.const import NotificationsMethods


class DeltaGetServerInformation(AlfaDBusMethod):

    __method_name__ = NotificationsMethods.GET_SERVER_INFORMATION

    def _invoke(self, param, invocation):
        name = GLib.Variant.new_string(kusozako_notifications.APPLICATION_NAME)
        vendor = GLib.Variant.new_string(kusozako_notifications.VENDOR_NAME)
        version = GLib.Variant.new_string(kusozako_notifications.VERSION)
        spec_version = GLib.Variant.new_string("1.2")
        value = GLib.Variant.new_tuple(name, vendor, version, spec_version)
        invocation.return_value(value)

# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .GetCapabilities import DeltaGetCapabilities
from .GetServerInformation import DeltaGetServerInformation
from .CloseNotification import DeltaCloseNotification
from .notify.Notify import DeltaNotify


class EchoMethods:

    def __init__(self, parent):
        DeltaGetCapabilities(parent)
        DeltaGetServerInformation(parent)
        DeltaCloseNotification(parent)
        DeltaNotify(parent)

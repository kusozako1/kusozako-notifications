# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GtkLayerShell
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals
from .content_area2.ContentArea import DeltaContentArea
from .DrawingArea import DeltaDrawingArea


class DeltaBalloon(Gtk.Window, DeltaEntity):

    def _on_realize(self, window):
        pixbuf = self._enquiry("delta > pixbuf")
        if pixbuf is None:
            left_margin = 10
        else:
            left_margin = pixbuf.get_width()
        window.set_size_request(540+48, 320)
        GtkLayerShell.init_for_window(window)
        GtkLayerShell.set_anchor(window, GtkLayerShell.Edge.LEFT, True)
        GtkLayerShell.set_margin(window, GtkLayerShell.Edge.LEFT, left_margin)
        GtkLayerShell.set_anchor(window, GtkLayerShell.Edge.BOTTOM, True)
        GtkLayerShell.set_layer(window, GtkLayerShell.Layer.OVERLAY)
        GtkLayerShell.set_keyboard_mode(
            window,
            GtkLayerShell.KeyboardMode.ON_DEMAND,
            )

    def _delta_call_add_overlay(self, widget):
        self._overlay.add_overlay(widget)

    def _delta_call_add_to_container(self, widget):
        self._overlay.add(widget)

    def receive_transmission(self, user_data):
        signal, message = user_data
        if signal != ApplicationSignals.QUEUE_MESSAGE:
            return
        visible = (message is not None)
        self.props.visible = visible
        if visible:
            self.show_all()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Window.__init__(self)
        self._overlay = Gtk.Overlay()
        self.add(self._overlay)
        DeltaDrawingArea(self)
        DeltaContentArea(self)
        self.connect("realize", self._on_realize)
        self._raise("delta > register application object", self)

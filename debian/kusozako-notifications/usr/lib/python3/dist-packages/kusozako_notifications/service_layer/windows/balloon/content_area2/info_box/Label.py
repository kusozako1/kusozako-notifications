# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals


class DeltaLabel(Gtk.Label, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, message = user_data
        if signal != ApplicationSignals.QUEUE_MESSAGE or message is None:
            return
        self.set_markup(message.markup)
        self.show()

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        Gtk.Label.__init__(
            self,
            use_markup=True,
            wrap=True,
            wrap_mode=Pango.WrapMode.WORD_CHAR,
            xalign=0.5,
            )
        scrolled_window.add(self)
        self._raise("delta > add to container", scrolled_window)
        self._raise("delta > register application object", self)

# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.Transmitter import FoxtrotTransmitter
from kusozako_notifications.const import NotificationsSpec
from .methods.Methods import EchoMethods


class DeltaDbusObject(DeltaEntity):

    @classmethod
    def new_for_dbus_connection(cls, parent, dbus_connection):
        dbus_object = cls(parent)
        dbus_object.construct(dbus_connection)

    def _on_method_call(self, *args):
        _, _, _, _, method, param, invocation = args
        user_data = method, param, invocation
        self._transmitter.transmit(user_data)

    def _delta_call_emit_signal(self, user_data):
        signal, param = user_data
        self._dbus_connection.emit_signal(
            NotificationsSpec.WELL_KNOWN_NAME,
            NotificationsSpec.OBJECT_PATH,
            NotificationsSpec.INTERFACE_NAME,
            signal,
            param
            )

    def _delta_call_register_dbus_method_object(self, object_):
        self._transmitter.register_listener(object_)

    def construct(self, dbus_connection):
        self._dbus_connection = dbus_connection
        dbus_connection.register_object(
            NotificationsSpec.OBJECT_PATH,
            NotificationsSpec.NOTIFICATIONS_INTERFACE,
            self._on_method_call,
            )

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        EchoMethods(self)

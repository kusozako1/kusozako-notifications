# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_notifications.Entity import DeltaEntity
from .info_box.InfoBox import DeltaInfoBox
from .button_box.ButtonBox import DeltaButtonBox


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            margin_top=24,
            margin_bottom=24,
            margin_start=48+24,
            margin_end=48,
            )
        DeltaInfoBox(self)
        DeltaButtonBox(self)
        self._raise("delta > add overlay", self)

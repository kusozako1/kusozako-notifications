# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf


class FoxtrotImageData:

    def _get_pixbuf_from_data(self, image_data):
        # Note:
        # image_data is GLib.Variant tuple
        # image_data[5] is number of channnel. it's needles.
        glib_bytes = image_data.get_child_value(6).get_data_as_bytes()
        return GdkPixbuf.Pixbuf.new_from_bytes(
            glib_bytes,                 # data as GLib.Bytes not bytes
            GdkPixbuf.Colorspace.RGB,   # color space
            image_data[3],              # has alpha
            image_data[4],              # bits per sample
            image_data[0],              # width
            image_data[1],              # height
            image_data[2],              # rowstride
            )

    def from_hints(self, hints):
        for key in ["image-data", "image_data", "icon_data"]:
            image_data = hints.lookup_value(key)
            if image_data is None:
                continue
            return self._get_pixbuf_from_data(image_data)
        return None

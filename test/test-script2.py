
import gi

gi.require_version('Notify', '0.7')

from gi.repository import Notify
from gi.repository import GdkPixbuf

PATH = "/home/takedanemuru/1119678.jpg"

Notify.init("foobar")
notify = Notify.Notification.new("summary", "body", "file")
notify.set_timeout(Notify.EXPIRES_DEFAULT)
notify.set_urgency(Notify.Urgency.CRITICAL)
pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(PATH, 256, 256, True)
notify.set_image_from_pixbuf(pixbuf)
notify.show()
Notify.uninit()


import gi

gi.require_version('Notify', '0.7')

from gi.repository import Notify

Notify.init("foobar")
notify = Notify.Notification.new("summary", "body", "dialog-warning")
notify.set_timeout(Notify.EXPIRES_DEFAULT)
notify.set_urgency(Notify.Urgency.NORMAL)
notify.show()
Notify.uninit()

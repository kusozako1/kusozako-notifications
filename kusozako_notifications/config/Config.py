# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako_notifications.Entity import DeltaEntity

APPLICATION_ID = "com.gitlab.kusozako1.Notifications"
NAMES = [GLib.get_user_config_dir(), APPLICATION_ID]
CONFIG_DIRECTORY = GLib.build_filenamev(NAMES)


class DeltaConfig(DeltaEntity):

    def _ensure_directory(self):
        if GLib.file_test(CONFIG_DIRECTORY, GLib.FileTest.EXISTS):
            return
        gfile = Gio.File.new_for_path(CONFIG_DIRECTORY)
        gfile.make_directory()

    def _ensure_config_file(self):
        names = [CONFIG_DIRECTORY, "application.conf"]
        target_path = GLib.build_filenamev(names)
        if not GLib.file_test(target_path, GLib.FileTest.EXISTS):
            directory = GLib.path_get_dirname(__file__)
            source_path = GLib.build_filenamev([directory, "template.config"])
            source_gfile = Gio.File.new_for_path(source_path)
            target_gfile = Gio.File.new_for_path(target_path)
            source_gfile.copy(target_gfile, Gio.FileCopyFlags.NONE)
        return target_path

    def get_config(self, group, key):
        try:
            value = self._key_file.get_value(group, key)
            return eval(value)
        except GLib.Error:
            return None

    def __init__(self, parent):
        self._parent = parent
        self._ensure_directory()
        path = self._ensure_config_file()
        self._key_file = GLib.KeyFile.new()
        self._key_file.load_from_file(path, GLib.KeyFileFlags.NONE)
        # edge = eval(self._key_file.get_value("general", "edge"))
        # print(edge, type(edge))

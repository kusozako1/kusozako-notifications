# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity


class AlfaGtkEventWatcher(DeltaEntity):

    EVENT_SOURCE_QUERY = "delta > event source"
    EVENT = "define event to watch here."

    def _on_watched(self, event_args):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        event_source = self._enquiry(self.EVENT_SOURCE_QUERY)
        event_source.connect(self.EVENT, self._on_watched)

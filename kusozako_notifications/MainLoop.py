# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity
from .application.Application import DeltaApplication
from .service_layer.ServiceLayer import DeltaServiceLayer
from .config.Config import DeltaConfig


class DeltaMainLoop(DeltaEntity):

    def _delta_info_config(self, user_data):
        group, key = user_data
        return self._config.get_config(group, key)

    def _delta_call_application_layer_ready(self, parent):
        self._config = DeltaConfig(parent)
        DeltaServiceLayer(parent)

    def __init__(self):
        self._parent = None
        DeltaApplication(self)

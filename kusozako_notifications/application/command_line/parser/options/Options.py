# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Version import DeltaVersion
from .dialog.Dialog import DeltaDialog


class EchoOptions:

    def __init__(self, parent):
        DeltaVersion(parent)
        DeltaDialog(parent)

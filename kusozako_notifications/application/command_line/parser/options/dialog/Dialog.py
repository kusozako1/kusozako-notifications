# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals
from .Message import FoxtrotMessage


class DeltaDialog(DeltaEntity):

    def execute(self, options_dict, option_value):
        message = FoxtrotMessage.new(options_dict, option_value)
        user_data = ApplicationSignals.ENQUEUE_MESSAGE, message
        self._raise("delta > application signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > add option", ("dialog", self))

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity
from .options.Options import EchoOptions


class DeltaParser(DeltaEntity):

    def _delta_call_add_option(self, user_data):
        key, option_entity = user_data
        self._options[key] = option_entity

    def parse(self, command_line):
        # options_dict is GLib.VariantDict
        options_dict = command_line.get_options_dict()
        for key in self._options:
            option_value = options_dict.lookup_value(key)
            if option_value is None:
                continue
            option_entity = self._options[key]
            option_entity.execute(options_dict, option_value)
            return False
        return True

    def __init__(self, parent):
        self._parent = parent
        self._options = {}
        EchoOptions(self)

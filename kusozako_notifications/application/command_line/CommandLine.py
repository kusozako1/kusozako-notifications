# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity
from .Builder import DeltaBuilder
from .parser.Parser import DeltaParser


class DeltaCommandLine(DeltaEntity):

    def _on_command_line(self, application, command_line):
        hold = self._parser.parse(command_line)
        if hold:
            application.hold()
        return 0

    def _delta_call_options_built(self, application):
        application.connect("command-line", self._on_command_line)

    def __init__(self, parent):
        self._parent = parent
        self._parser = DeltaParser(self)
        DeltaBuilder(self)

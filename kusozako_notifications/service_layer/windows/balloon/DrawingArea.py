# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_notifications.Entity import DeltaEntity
from . import Painter


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _on_draw(self, drawing_area, cairo_context):
        cairo_context.set_source_rgba(255/255, 20/255, 147/255, 0.5)
        Painter.paint(cairo_context)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self, hexpand=True, vexpand=True)
        self.connect("draw", self._on_draw)
        self._raise("delta > add to container", self)

# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Gtk
from gi.repository import GdkPixbuf
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals


class DeltaImage(Gtk.Image, DeltaEntity):

    def _set_image_from_mesage(self, message):
        if isinstance(message.icon, Gio.ThemedIcon):
            self.set_from_gicon(message.icon, Gtk.IconSize.INVALID)
        elif isinstance(message.icon, GdkPixbuf.Pixbuf):
            self.set_from_pixbuf(message.icon)

    def receive_transmission(self, user_data):
        signal, message = user_data
        if signal != ApplicationSignals.QUEUE_MESSAGE or message is None:
            return
        self._set_image_from_mesage(message)
        self.props.visible = (message.icon is not None)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(
            self,
            pixel_size=128,
            xalign=1,
            yalign=0.5,
            )
        self._raise("delta > add to container", self)
        self._raise("delta > register application object", self)

# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals


class DeltaExtraButton(Gtk.Button, DeltaEntity):

    @classmethod
    def new_for_data(cls, parent, data):
        instance = cls(parent)
        instance.construct(data)

    def _execute_action(self, action):
        command = action.split(" ")
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        subprocess.wait_async(None, None, None)

    def _on_clicked(self, button, action=None):
        if action is not None:
            self._execute_action(action)
        user_data = ApplicationSignals.DEQUEUE_MESSAGE, None
        self._raise("delta > application signal", user_data)

    def construct(self, data):
        self.set_label(data.pop(0))
        action = data.pop(0) if len(data) > 0 else None
        self.connect("clicked", self._on_clicked, action)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            margin_end=8,
            relief=Gtk.ReliefStyle.NONE,
            hexpand=True,
            )
        self._raise("delta > add to container", self)

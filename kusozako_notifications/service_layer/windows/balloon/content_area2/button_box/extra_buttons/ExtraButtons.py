# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals
from .ExtraButton import DeltaExtraButton


class DeltaExtraButtons(Gtk.Box, DeltaEntity):

    def _clear_children(self):
        for button in self.get_children():
            button.destroy()

    def _set_extra_buttons(self, message):
        data = message.extra_buttons.copy()
        while len(data) > 0:
            DeltaExtraButton.new_for_data(self, data)
        self.show_all()

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def receive_transmission(self, user_data):
        signal, message = user_data
        if signal != ApplicationSignals.QUEUE_MESSAGE or message is None:
            return
        self._clear_children()
        self._set_extra_buttons(message)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self._raise("delta > add to container", self)
        self._raise("delta > register application object", self)

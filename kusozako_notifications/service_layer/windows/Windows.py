# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity
from .persona.Persona import DeltaPersona
from .balloon.Balloon import DeltaBalloon
from .Pixbuf import DeltaPixbuf


class DeltaWindows(DeltaEntity):

    def _delta_info_pixbuf(self):
        return self._pixbuf.get_pixbuf()

    def show_windows(self):
        self._persona.show()
        self._balloon.show()

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = DeltaPixbuf(self)
        self._persona = DeltaPersona(self)
        self._balloon = DeltaBalloon(self)

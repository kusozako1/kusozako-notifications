# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import NotificationsSpec
from .dbus_object.DBusObject import DeltaDbusObject


class DeltaNotifications(DeltaEntity):

    def _on_name_aquired(self, dbus_connection, name):
        DeltaDbusObject.new_for_dbus_connection(self, dbus_connection)

    def _on_bus_get(self, cancellable, task):
        dbus_connection = Gio.bus_get_finish(task)
        Gio.bus_own_name_on_connection(
            dbus_connection,
            NotificationsSpec.WELL_KNOWN_NAME,
            Gio.BusNameOwnerFlags.NONE,
            self._on_name_aquired
            )

    def __init__(self, parent):
        self._parent = parent
        Gio.bus_get(Gio.BusType.SESSION, None, self._on_bus_get)

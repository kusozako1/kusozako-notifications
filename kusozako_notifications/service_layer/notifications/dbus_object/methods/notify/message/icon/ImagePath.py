# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GdkPixbuf


class FoxtrotImagePath:

    def from_hints(self, hints):
        path_data = hints.lookup_value("image-path")
        if path_data is None:
            return None
        path = GLib.Variant.get_string(path_data)
        return GdkPixbuf.Pixbuf.new_from_file(path)

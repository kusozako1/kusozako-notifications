# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GdkPixbuf
from kusozako_notifications.const import NotifyParam


class FoxtrotApplicationIcon:

    def from_param(self, param):
        data = param.get_child_value(NotifyParam.APP_ICON)
        app_icon = GLib.Variant.get_string(data)
        if not app_icon:
            return None
        if GLib.file_test(app_icon, GLib.FileTest.EXISTS):
            param = app_icon, 128, 128, True
            return GdkPixbuf.Pixbuf.new_from_file_at_scale(*param)
        return Gio.Icon.new_for_string(app_icon)

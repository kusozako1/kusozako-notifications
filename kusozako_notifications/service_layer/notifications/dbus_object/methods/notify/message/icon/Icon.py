# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.const import NotifyParam
from .ImageData import FoxtrotImageData
from .ImagePath import FoxtrotImagePath
from .ApplicationIcon import FoxtrotApplicationIcon


class FoxtrotIcon:

    @classmethod
    def get_default(cls):
        if "_default" not in dir(cls):
            cls._default = cls()
        return cls._default

    def build_(self, param):
        hints = param.get_child_value(NotifyParam.HINTS)
        icon = self._image_data.from_hints(hints)
        if icon is not None:
            return icon
        icon = self._image_path.from_hints(hints)
        if icon is not None:
            return icon
        return self._application_icon.from_param(param)

    def __init__(self):
        self._image_data = FoxtrotImageData()
        self._image_path = FoxtrotImagePath()
        self._application_icon = FoxtrotApplicationIcon()

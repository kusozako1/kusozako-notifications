# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_notifications.const import NotifyParam


class FoxtrotInvocationClosure:

    _unique_id = 1000000

    @classmethod
    def new(cls, param, invocation):
        id_as_gvariant = param.get_child_value(NotifyParam.ID)
        id_ = id_as_gvariant.get_uint32()
        if id_ == 0:
            cls._unique_id += 1
            id_ = cls._unique_id
        return cls(id_, invocation)

    def get_id(self):
        return self._id

    def invoke(self):
        self._invocation.return_value(self._id_as_return_value)

    def __init__(self, id_, invocation):
        self._id = id_
        id_guint32 = GLib.Variant.new_uint32(id_)
        self._id_as_return_value = GLib.Variant.new_tuple(id_guint32)
        self._invocation = invocation

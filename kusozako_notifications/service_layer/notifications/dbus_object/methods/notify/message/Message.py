# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.alfa.MessageEntity import AlfaMessageEntity
from kusozako_notifications.const import NotifyParam
from .InvocationClosure import FoxtrotInvocationClosure
from . import Markup
from .icon.Icon import FoxtrotIcon


class FoxtrotMessage(AlfaMessageEntity):

    @classmethod
    def new(cls, param, invocation):
        instance = cls()
        instance.construct(param, invocation)
        return instance

    def close(self):
        self._invocation.invoke()

    def construct(self, param, invocation):
        icon_builder = FoxtrotIcon.get_default()
        self._icon = icon_builder.build_(param)
        self._invocation = FoxtrotInvocationClosure.new(param, invocation)
        self._markup = Markup.new_for_param(param)
        hints = param.get_child_value(NotifyParam.HINTS)
        self._urgency = hints.lookup_value("urgency").get_byte()

    def get_id(self):
        return self._invocation.get_id()

    @property
    def icon(self):
        return self._icon

    @property
    def markup(self):
        return self._markup

    @property
    def urgency(self):
        return self._urgency

# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.const import NotifyParam

MARKUP_FORMAT = "<b>{} : {}</b>\n\n{}"


def new_for_param(param):
    return MARKUP_FORMAT.format(
        param.get_child_value(NotifyParam.APP_NAME).get_string(),
        param.get_child_value(NotifyParam.SUMMARY).get_string(),
        param.get_child_value(NotifyParam.BODY).get_string()
        )

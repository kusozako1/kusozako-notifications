# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.alfa.DBusMethod import AlfaDBusMethod
from kusozako_notifications.const import ApplicationSignals
from kusozako_notifications.const import NotificationsMethods
from .message.Message import FoxtrotMessage


class DeltaNotify(AlfaDBusMethod):

    __method_name__ = NotificationsMethods.NOTIFY

    def _invoke(self, param, invocation):
        message = FoxtrotMessage.new(param, invocation)
        user_data = ApplicationSignals.ENQUEUE_MESSAGE, message
        self._raise("delta > application signal", user_data)

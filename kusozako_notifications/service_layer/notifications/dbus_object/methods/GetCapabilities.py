# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_notifications.alfa.DBusMethod import AlfaDBusMethod
from kusozako_notifications.const import NotificationsMethods

CAPABILITIES = ["body", "body-hyperlinks", "persistence"]


class DeltaGetCapabilities(AlfaDBusMethod):

    __method_name__ = NotificationsMethods.GET_CAPABILITIES

    def _invoke(self, param, invocation):
        capabilities = GLib.Variant.new_strv(CAPABILITIES)
        value = GLib.Variant.new_tuple(capabilities,)
        invocation.return_value(value)

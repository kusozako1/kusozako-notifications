# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.alfa.DBusMethod import AlfaDBusMethod
from kusozako_notifications.const import NotificationsMethods


class DeltaCloseNotification(AlfaDBusMethod):

    __method_name__ = NotificationsMethods.CLOSE_NOTIFICATION

    def _invoke(self, param, invocation):
        pass

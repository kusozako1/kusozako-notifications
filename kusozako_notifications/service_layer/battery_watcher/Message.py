# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.alfa.MessageEntity import AlfaMessageEntity
from kusozako_notifications.const import BatteryState

TEMPLATE = """<span font-weight='bold'>"Battery State Changed"

{}</span>"""


class FoxtrotMessage(AlfaMessageEntity):

    @classmethod
    def new(cls, state):
        instance = cls()
        instance.construct(state)
        return instance

    def construct(self, state):
        battery_message = BatteryState.MESSAGES[state]
        self._markup = TEMPLATE.format(battery_message)

    @property
    def markup(self):
        return self._markup

    @property
    def extra_buttons(self):
        return []

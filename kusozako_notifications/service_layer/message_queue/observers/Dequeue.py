# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals


class DeltaDequeue(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != ApplicationSignals.DEQUEUE_MESSAGE:
            return
        self._raise("delta > remove message", None)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register application object", self)

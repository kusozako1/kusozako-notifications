# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals


class DeltaEnqueue(DeltaEntity):

    def _timeout(self, message):
        self._raise("delta > remove message", message)
        return GLib.SOURCE_REMOVE

    def receive_transmission(self, user_data):
        signal, message = user_data
        if signal != ApplicationSignals.ENQUEUE_MESSAGE:
            return
        self._raise("delta > add message", message)
        GLib.timeout_add_seconds(30, self._timeout, message)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register application object", self)

# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GSound
from kusozako_notifications.Entity import DeltaEntity
from kusozako_notifications.const import ApplicationSignals
from kusozako_notifications.const import NotifyUrgency

SOUNDS = {
    NotifyUrgency.LOW: "dialog-information",
    NotifyUrgency.NORMAL: "dialog-warning",
    NotifyUrgency.CRITICAL: "alarm-clock-elapsed",
}


class DeltaSoundContext(DeltaEntity):

    def _on_message_received(self, message):
        try:
            sound = SOUNDS.get(message.urgency, "dialog-warning")
            self._context.play_simple({GSound.ATTR_EVENT_ID: sound})
        except GLib.GError:
            self._context.play_simple({GSound.ATTR_EVENT_ID: "dialog-warning"})

    def receive_transmission(self, user_data):
        signal, message = user_data
        if signal != ApplicationSignals.QUEUE_MESSAGE or message is None:
            return
        self._on_message_received(message)

    def __init__(self, parent):
        self._parent = parent
        self._context = GSound.Context.new()
        self._raise("delta > register application object", self)

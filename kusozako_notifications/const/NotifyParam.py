# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

APP_NAME = 0            # string
ID = 1                  # uint32
APP_ICON = 2            # string
SUMMARY = 3             # string
BODY = 4                # string
ACTIONS = 5             # Array of [String] as key-value pair
HINTS = 6               # Dict of {String: Variant}
EXPIRE_TIMEOUT = 7      # int32
